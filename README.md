# Ed-Fi Deployment #

Google Cloud Deployment scripts for deploying the Ed-Fi technology stack on Google Cloud Platform.


```bash
    ./deploy.sh PROJECT-NAME
```

Licensed under the [Ed-Fi
License](https://www.ed-fi.org/assets/2017/07/Ed-Fi-Alliance-Adopter-License-and-Source-Code-Agreement-1-7-14-SAMPLE.pdf).
