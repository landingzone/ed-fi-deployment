# What is kubeIP?

Many applications need to be whitelisted by users based on a Source IP Address. As of today, Google Kubernetes Engine doesn't support assigning a static pool of IP addresses to the GKE cluster. Using kubeIP, this problem is solved by assigning GKE nodes external IP addresses from a predefined list. kubeIP monitors the Kubernetes API for new/removed nodes and applies the changes accordingly.

# Landing Zone Instructions
1. Activate Cloud Shell in the district/cmo's Google Project.
2. Clone or pull updates for https://bitbucket.org/landingzone/ed-fi-deployment
3. `cd ~/ed-fi-deployment/kubernetes/kube_ip_lz_deploy/`
4. Run the script. It will prompt you for some input. It will do all of the work needed 
to deploy the static IP addresses and the Kubernetes deployment. `sh deploy_kubeip.sh`
5. You may see some errors as it's deploying. This is usually ok because some resources
deployed by this script may have already existed.
6. Once the script has run it can take ~5 minutes for the Kubernetes cluster to re-stabalize.
Check that kubeip works by checking that the External IP address in the Google Cloud console
(VPC Network --> External IP addresses) have been assigned to the cluster VMs. There should no
longer be any static VMs listed.

# Deploy kubeIP (without building from source)

If you just want to use kubeIP (instead of building it yourself from source), please follow the instructions in this section. You’ll need Kubernetes version 1.10 or newer. You'll also need the Google Cloud SDK. You can install the [Google Cloud SDK](https://cloud.google.com/sdk) (which also installs kubectl).

To configure your Google Cloud SDK, set default project as:

```
gcloud config set project {your project_id}
```

Set the environment variables: 
 
```
export GCP_REGION=<gcp-region>
export GCP_ZONE=<gcp-zone>
export GKE_CLUSTER_NAME=<cluster-name>
export PROJECT_ID=$(gcloud config list --format 'value(core.project)')
export KUBEIP_NODEPOOL=<nodepool-with-static-ips>
export KUBEIP_SELF_NODEPOOL=<nodepool-for-kubeip-to-run-in>
```

**Creating an IAM Service Account and obtaining the Key in JSON format**

Create a Service Account with this command: 

```
gcloud iam service-accounts create kubeip-service-account --display-name "kubeIP"
```

Create and attach a custom kubeIP role to the service account by running the following commands:

```
gcloud iam roles create kubeip --project $PROJECT_ID --file roles.yaml

gcloud projects add-iam-policy-binding $PROJECT_ID --member serviceAccount:kubeip-service-account@$PROJECT_ID.iam.gserviceaccount.com --role projects/$PROJECT_ID/roles/kubeip
```

Generate the Key using the following command:

```
gcloud iam service-accounts keys create key.json \
--iam-account kubeip-service-account@$PROJECT_ID.iam.gserviceaccount.com
```
 
**Create Kubernetes Secret Objects**

Get your GKE cluster credentaials with (replace *cluster_name* with your real GKE cluster name):

<pre>
gcloud container clusters get-credentials $GKE_CLUSTER_NAME \
--region $GCP_ZONE \
--project $PROJECT_ID
</pre> 

Create a Kubernetes secret object by running:

```
kubectl create secret generic kubeip-key --from-file=key.json -n kube-system
```
Get RBAC permissions with:
```
kubectl create clusterrolebinding cluster-admin-binding \
   --clusterrole cluster-admin --user `gcloud config list --format 'value(core.account)'`
```
**Create Static, Reserved IP Addresses:** 

Create as many static IP addresses for the number of nodes in your GKE cluster (this example creates 10 addresses) so you will have enough addresses when your cluster scales up (manually or automatically):

```
for i in {1..10}; do gcloud compute addresses create kubeip-ip$i --project=$PROJECT_ID --region=$GCP_REGION; done
```

Add labels to reserved IP addresses. A common practice is to assign a unique value per cluster (for example cluster name):

```
for i in {1..10}; do gcloud beta compute addresses update kubeip-ip$i --update-labels kubeip=$GKE_CLUSTER_NAME --region $GCP_REGION; done
```

<pre>
{
  sed -i "s/reserved/$GKE_CLUSTER_NAME/g" deploy/kubeip-configmap.yaml
  sed -i "s/default-pool/$KUBEIP_NODEPOOL/g" deploy/kubeip-configmap.yaml
}
</pre>

Make sure the `deploy/kubeip-configmap.yaml` file contains the correct values:

 - The `KUBEIP_LABELVALUE` should be your GKE's cluster name
 - The `KUBEIP_NODEPOOL` should match the name of your GKE node-pool on which kubeIP will operate
 - The `KUBEIP_FORCEASSIGNMENT` - controls whether kubeIP should assign static IPs to existing nodes in the node-pool and defaults to true

We recommend that KUBEIP_NODEPOOL should *NOT* be the same as KUBEIP_SELF_NODEPOOL


If you would like to assign addresses to other node pools, then `KUBEIP_NODEPOOL` can be added to this nodepool `KUBEIP_ADDITIONALNODEPOOLS` as a comma separated list.
You should tag the addresses for this pool with the `KUBEIP_LABELKEY` value + `-node-pool` and assign the value of the node pool a name i.e.,  `kubeip-node-pool=my-node-pool`

<pre>
sed -i "s/pool-kubip/$KUBEIP_SELF_NODEPOOL/g" deploy/kubeip-deployment.yaml
</pre>

Deploy kubeIP by running: 

```
kubectl apply -f deploy/.
```

Once you’ve assigned an IP address to a node kubeIP, a label will be created for that node `kubip_assigned` with the value of the IP address (`.` are replaced with `_`):

 `172.31.255.255 ==> 172_31_255_255`


References:

 - Event listening code was take from [kubewatch](https://github.com/bitnami-labs/kubewatch/)
