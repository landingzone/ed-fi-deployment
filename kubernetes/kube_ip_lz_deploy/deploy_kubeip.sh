#!/usr/bin/env bash

confirm() {
#
# syntax: confirm [<prompt>]
#
# Prompts the user to enter Yes or No and returns 0/1.
#
# This  program  is free software: you can redistribute it and/or modify  it
# under the terms of the GNU General Public License as published by the Free
# Software  Foundation, either version 3 of the License, or (at your option)
# any later version.
#
# This  program  is  distributed  in the hope that it will  be  useful,  but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public  License
# for more details.
#
# You  should  have received a copy of the GNU General Public License  along
# with this program. If not, see <http://www.gnu.org/licenses/>
#
#  04 Jul 17   0.1   - Initial version - MEJT
#
  local _prompt _default _response

  if [ "$1" ]; then _prompt="$1"; else _prompt="Are you sure"; fi
  _prompt="$_prompt [y/n] ?"

  # Loop forever until the user enters a valid response (Y/N or Yes/No).
  while true; do
    read -r -p "$_prompt " _response
    case "$_response" in
      [Yy][Ee][Ss]|[Yy]) # Yes or Y (case-insensitive).
        return 0
        ;;
      [Nn][Oo]|[Nn])  # No or N.
        return 1
        ;;
      *) # Anything else (including a blank) is invalid.
        ;;
    esac
  done
}

echo This script will deploy kubeip to a GKE nodepool with a single micro instance.

PROJECT_ACCOUNT=$(gcloud config list account --format "value(core.account)")
PROJECT_ID=$(gcloud config list --format 'value(core.project)')

echo '\nThe work will be done for the following account and project within that account:'
echo '\tAccount: '$PROJECT_ACCOUNT
echo '\tProject Id: '$PROJECT_ID

confirm 'Is this correct'

if [ $? -eq 1 ]
then echo 'Use "gcloud auth login" and "gcloud config set project" to set the correct values and rerun this script'; exit 0
fi

echo '\nPlease enter the following information to deploy: '
read -p 'GCP Region: ' GCP_REGION
read -p 'GCP Zone: ' GCP_ZONE
read -p 'GKE Cluster Name: ' GKE_CLUSTER_NAME
# read -p 'Keyfile Prefix (should indicate LEA): ' KEYFILE_PREFIX
# read -p 'Node pool that needs static ips (the composer nodepool): ' KUBEIP_NODEPOOL
# read -p 'Node pool to run KUBEIP in (a separate nodepool): ' KUBEIP_SELF_NODEPOOL

KUBEIP_NODEPOOL='default-pool'

# trying to run it in the default pool instead of a new nodepool
KUBEIP_SELF_NODEPOOL='default-pool'

## Create a nodepool for KUBEIP:
#KUBEIP_SELF_NODEPOOL='kubeip'
#gcloud container node-pools create $KUBEIP_SELF_NODEPOOL \
#--cluster $GKE_CLUSTER_NAME \
#--disk-size 10GB \
#--disk-type pd-standard \
#--enable-autorepair \
#--enable-autoupgrade \
#--image-type COS \
#--machine-type e2-micro \
#--num-nodes 1 \
#--zone us-central1-c

# create service account for kube ip
gcloud iam service-accounts create kubeip-service-account --display-name "kubeIP"

# Create and attach a custom kubeIP role to the service account
gcloud iam roles create kubeip --project $PROJECT_ID --file roles.yaml

gcloud projects add-iam-policy-binding $PROJECT_ID --member serviceAccount:kubeip-service-account@$PROJECT_ID.iam.gserviceaccount.com --role projects/$PROJECT_ID/roles/kubeip

# Generate the keyfile for the service account
# keyfile_name="${KEYFILE_PREFIX}_key.json"
gcloud iam service-accounts keys create key.json \
--iam-account kubeip-service-account@$PROJECT_ID.iam.gserviceaccount.com

# CREATE KUBERNETES SECRET OBJECTS

# Get the GKE credentials
gcloud container clusters get-credentials $GKE_CLUSTER_NAME \
--region $GCP_ZONE \
--project $PROJECT_ID

# Create a Kubernetes secret object
kubectl create secret generic kubeip-key --from-file=key.json -n kube-system

# Get RBAC permissions
kubectl create clusterrolebinding cluster-admin-binding \
   --clusterrole cluster-admin --user `gcloud config list --format 'value(core.account)'`

# create static external IP addresses. These commands will fail if they already exist and that's ok
description="Dedicated IP for a compute engine VM in the default node pool for the GKE cluster for Cloud Composer. This IP address will be whitelisted by vendors so Landing Zone can access their resources."
gcloud compute addresses create airflow001 --project=$PROJECT_ID --description='${description}' --region=$GCP_REGION
gcloud compute addresses create airflow002 --project=$PROJECT_ID --description='${description}' --region=$GCP_REGION
gcloud compute addresses create airflow003 --project=$PROJECT_ID --description='${description}' --region=$GCP_REGION

# Assign a kubeip label to existing external ip addresses with Airflow name
gcloud beta compute addresses update airflow --update-labels kubeip=$GKE_CLUSTER_NAME --region $GCP_REGION
gcloud beta compute addresses update airflow001 --update-labels kubeip=$GKE_CLUSTER_NAME --region $GCP_REGION
gcloud beta compute addresses update airflow002 --update-labels kubeip=$GKE_CLUSTER_NAME --region $GCP_REGION
gcloud beta compute addresses update airflow003 --update-labels kubeip=$GKE_CLUSTER_NAME --region $GCP_REGION

{
  sed -i.bak "s/reserved/$GKE_CLUSTER_NAME/g" deploy/kubeip-configmap.yaml && rm deploy/kubeip-configmap.yaml.bak
  sed -i.bak "s/default-pool/$KUBEIP_NODEPOOL/g" deploy/kubeip-configmap.yaml && rm deploy/kubeip-configmap.yaml.bak
}

sed -i.bak "s/pool-kubip/$KUBEIP_SELF_NODEPOOL/g" deploy/kubeip-deployment.yaml && rm deploy/kubeip-deployment.yaml.bak

kubectl apply -f deploy/.
