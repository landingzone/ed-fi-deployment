#!/bin/bash

echo "Setting project id to $1"
gcloud config set project $1

echo "Enabling Compute Engine API"
gcloud services enable compute.googleapis.com

echo "Enabling Deploment Manager API"
gcloud services enable deploymentmanager.googleapis.com

echo "Enabling Cloud SQL Admin API"
gcloud services enable sqladmin.googleapis.com

echo "Deploying solution!"
gcloud deployment-manager deployments create edfi --config deployment-scripts/edfi_technology.yaml
