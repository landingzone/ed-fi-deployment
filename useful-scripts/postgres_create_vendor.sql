CREATE EXTENSION pgcrypto;

DO $$
<<edfi_block>>
DECLARE
	vendor_id integer;
	vendor_name varchar := 'Vendor Name';
	application_id integer;
	application_name varchar := 'Application Name';
	claim_set_name varchar := 'District Hosted SIS Vendor';
	education_organization_id integer := 123456789;
	namespace varchar := 'uri://ed-fi.org';
	operational_context_uri varchar := 'uri://ed-fi.org';
	client_key varchar;
	client_secret varchar;
BEGIN

	-- add vendor
	INSERT INTO dbo.vendors (vendorname) VALUES (vendor_name);

	RAISE NOTICE 'Vendor % added.', vendor_name;

	-- get generated vendor id of new vendor
	SELECT vendors.vendorid INTO vendor_id FROM dbo.vendors WHERE vendors.vendorname = vendor_name;

	RAISE NOTICE 'Vendor assiged VendorId %', vendor_id;

	-- create namespace for new vendor
	INSERT INTO dbo.vendornamespaceprefixes (
		namespaceprefix,
		vendor_vendorid
	)
	SELECT
		namespace,
		vendors.vendorid
	FROM dbo.vendors
	WHERE vendors.vendorid = vendor_id;

	RAISE NOTICE 'Vendor given namespace %.', namespace;

	-- create application
	INSERT INTO dbo.applications (
        applicationname,
        operationalcontexturi,
        vendor_vendorid,
        claimsetname
	)
	SELECT
		application_name,
		operational_context_uri,
		vendor_id,
		claim_set_name
	FROM dbo.vendors;

	RAISE NOTICE 'Application % added.', application_name;

	-- get generated application id of new application
	SELECT applications.applicationid INTO application_id FROM dbo.applications WHERE applications.applicationname = application_name;

	RAISE NOTICE 'Application assiged ApplicationId %', application_id;

	-- create education organization
	INSERT INTO dbo.applicationeducationorganizations (
        educationorganizationid,
        application_applicationid
	)
	SELECT
		education_organization_id,
		application_id
	FROM  dbo.applications
	WHERE applications.applicationname = application_name;

	-- generate client key and secret
	SELECT REPLACE(SUBSTRING(CAST(gen_random_uuid() AS VARCHAR), 0, 20), '-', '') INTO client_key;
	SELECT REPLACE(SUBSTRING(CAST(gen_random_uuid() AS VARCHAR), 0, 24), '-', '') INTO client_secret;

	RAISE NOTICE 'Client key created: %', client_key;
	RAISE NOTICE 'Client secret created: %', client_secret;

	INSERT INTO dbo.apiclients (
	    key,
        secret,
        name,
        isapproved,
        usesandbox,
        sandboxtype,
        secretishashed,
        application_applicationid
	)
	SELECT
		client_key,
		client_secret,
		vendor_name,
		true,
		false,
		0,
		false,
		application_id
	FROM dbo.applications;

	INSERT INTO dbo.apiclientapplicationeducationorganizations (
        apiclient_apiclientid,
        applicationedorg_applicationedorgid
	)
	SELECT
	    apiclients.apiclientid,
	    applicationeducationorganizations.applicationeducationorganizationid
	FROM dbo.apiclients
	CROSS JOIN dbo.applications
	INNER JOIN dbo.applicationeducationorganizations ON
	    applications.applicationid = applicationeducationorganizations.application_applicationid
	WHERE apiclients.name = vendor_name AND applications.applicationname = application_name;

	RAISE NOTICE 'Associated API client with ed org';

END edfi_block $$;
