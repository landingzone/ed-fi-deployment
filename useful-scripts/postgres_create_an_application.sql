DO $$
<<edfi_block>>
DECLARE
		-- Creates an application, sets up the necessary associations and logs the key and secret.
		-- Assumes that the vendor is already created.
		-- Assumes that the claimset is already created.
    -- Run this query from the EdFi_Admin database.
	  claim_set_name VARCHAR := 'Landing Zone Elevated Permissions';  -- Claimset you want to give the application
    vendor_name VARCHAR := 'Landing Zone';   -- Vendor to create the application under
    vendor_id INTEGER;
    application_name VARCHAR := 'Landing Zone Maintenance';   -- Application to be created
    application_id INTEGER;
    -- DISCLAIMER! Couldn't figure out the loop through an array so this query will do two associations - will need to modify if doing more or less
    education_organization_id_1 INTEGER := 3601085;
    education_organization_id_2 INTEGER := 294;
    client_key varchar;
    client_secret varchar;
    api_client_id integer;
BEGIN

    -- Set the vendor_id variable of the parent vendor for the new application
	SELECT vendorid INTO vendor_id
	FROM dbo.vendors
    WHERE vendorname = vendor_name;

    -- Create the application
    INSERT INTO dbo.applications (applicationname, vendor_vendorid, claimsetname, operationalcontexturi)
    VALUES (application_name, vendor_id, claim_set_name, 'uri://ed-fi.org');
    RAISE NOTICE 'Application % added.', application_name;

    -- Set the application_id variable of the recently created application
	SELECT applicationid INTO application_id
	FROM dbo.applications
    WHERE applicationname = application_name;

    -- Create application education organization association
	INSERT INTO dbo.applicationeducationorganizations (educationorganizationid, application_applicationid)
	VALUES (education_organization_id_1, application_id);
    RAISE NOTICE 'App associated to % ed org', education_organization_id_1;
    	INSERT INTO dbo.applicationeducationorganizations (educationorganizationid, application_applicationid)
	VALUES (education_organization_id_2, application_id);
    RAISE NOTICE 'App associated to % ed org', education_organization_id_2;

	-- Generate client key and secret
	SELECT REPLACE(SUBSTRING(CAST(gen_random_uuid() AS VARCHAR), 0, 20), '-', '') INTO client_key;
	SELECT REPLACE(SUBSTRING(CAST(gen_random_uuid() AS VARCHAR), 0, 24), '-', '') INTO client_secret;
	RAISE NOTICE 'Client key created: %', client_key;
	RAISE NOTICE 'Client secret created: %', client_secret;

    -- Create apiclient for our new application
	INSERT INTO dbo.apiclients (
	    key,
        secret,
        name,
        isapproved,
        usesandbox,
        sandboxtype,
        secretishashed,
        application_applicationid
	)
	SELECT
		client_key,
		client_secret,
		vendor_name,
		true,
		false,
		0,
		false,
		application_id
	FROM dbo.applications
    WHERE applicationid = application_id;
    RAISE NOTICE 'Api client created';

    -- Get the newly created apiclient id
    SELECT apiclientid INTO api_client_id
    FROM apiclients
    WHERE application_applicationid = application_id;

    -- Create the apiclient application ed org association
	INSERT INTO dbo.apiclientapplicationeducationorganizations (
        apiclient_apiclientid,
        applicationedorg_applicationedorgid
	)
	SELECT api_client_id, applicationeducationorganizationid
    FROM applicationeducationorganizations
    WHERE application_applicationid = application_id;
	RAISE NOTICE 'Associated API client with ed org';

END edfi_block $$;
