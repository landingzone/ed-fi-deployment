DO $$
<<edfi_block>>
DECLARE
	-- Creates a claimset with elevated permissions capable of deleting records with no associations
	-- DISCLAIMER!!! Make sure that the resourceclaimids listed in this query match the ODS you are working in (especially the school one)
	claim_set_name varchar := 'Landing Zone Elevated Permissions';
  claim_set_id integer;
BEGIN

    -- clean up any existing prior versions of the claimset
    DELETE FROM dbo.claimsetresourceclaims WHERE claimset_claimsetid = (
        SELECT claimsetid FROM dbo.claimsets WHERE claimsetname = claim_set_name);

    DELETE FROM dbo.claimsets WHERE claimsetname = claim_set_name;

	INSERT INTO dbo.claimsets (claimsetname, application_applicationid)
	VALUES (claim_set_name, 1);  -- 1 is the application ID for the Ed-Fi ODS API

    -- Set the claim_set_id variable of the newly created claimset
	SELECT claimsetid INTO claim_set_id
	FROM dbo.claimsets
    WHERE claimsetname = claim_set_name;

	-- create
	INSERT INTO dbo.claimsetresourceclaims (
        action_actionid,
        claimset_claimsetid,
	    resourceclaim_resourceclaimid,
	    authorizationstrategyoverride_authorizationstrategyid
	)
	SELECT
	    1, -- create
	    claim_set_id,
	    resourceclaims.resourceclaimid,
	    1 -- NoFurtherAuthorizationRequired
	FROM dbo.resourceclaims
	WHERE resourceclaimid IN (
        207, -- school
        5, -- people (student, staff, parent)
        6, -- relationshipBasedData (various endpoints that rely on primaryRelationships - maybe?)
        10 -- primaryRelationships (staff edorg association and student school associations)
    );

    -- read
	INSERT INTO dbo.claimsetresourceclaims (
        action_actionid,
        claimset_claimsetid,
	    resourceclaim_resourceclaimid,
	    authorizationstrategyoverride_authorizationstrategyid
	)
	SELECT
	    2, -- read
	    claim_set_id,
	    resourceclaims.resourceclaimid,
	    1 -- NoFurtherAuthorizationRequired
	FROM dbo.resourceclaims
	WHERE resourceclaimid IN (
        207, -- school
        5, -- people (student, staff, parent)
        6, -- relationshipBasedData (various endpoints that rely on primaryRelationships - maybe?)
        10 -- primaryRelationships (staff edorg association and student school associations)
    );

	-- update
	INSERT INTO dbo.claimsetresourceclaims (
        action_actionid,
        claimset_claimsetid,
	    resourceclaim_resourceclaimid,
	    authorizationstrategyoverride_authorizationstrategyid
	)
	SELECT
	    3, -- update
	    claim_set_id,
	    resourceclaims.resourceclaimid,
	    1 -- NoFurtherAuthorizationRequired
	FROM dbo.resourceclaims
	WHERE resourceclaimid IN (
        207, -- school
        5, -- people (student, staff, parent)
        6, -- relationshipBasedData (various endpoints that rely on primaryRelationships - maybe?)
        10 -- primaryRelationships (staff edorg association and student school associations)
    );

    -- delete
	INSERT INTO dbo.claimsetresourceclaims (
        action_actionid,
        claimset_claimsetid,
	    resourceclaim_resourceclaimid,
	    authorizationstrategyoverride_authorizationstrategyid
	)
	SELECT
	    4, -- delete
	    claim_set_id,
	    resourceclaims.resourceclaimid,
	    1 -- NoFurtherAuthorizationRequired
	FROM dbo.resourceclaims
	WHERE resourceclaimid IN (
        207, -- school
        5, -- people (student, staff, parent)
        6, -- relationshipBasedData (various endpoints that rely on primaryRelationships - maybe?)
        10 -- primaryRelationships (staff edorg association and student school associations)
    );

END edfi_block $$;
