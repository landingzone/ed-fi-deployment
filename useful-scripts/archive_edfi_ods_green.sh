# Script to archive the ODS and create a fresh one for the next school year
# TODO - remove unnecessary exports

# exit when any command fails
set -e

###################
# Helper Functions
###################
confirm() {
#
# syntax: confirm [<prompt>]
#
# Prompts the user to enter Yes or No and returns 0/1.
#
# This  program  is free software: you can redistribute it and/or modify  it
# under the terms of the GNU General Public License as published by the Free
# Software  Foundation, either version 3 of the License, or (at your option)
# any later version.
#
# This  program  is  distributed  in the hope that it will  be  useful,  but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public  License
# for more details.
#
# You  should  have received a copy of the GNU General Public License  along
# with this program. If not, see <http://www.gnu.org/licenses/>
#
#  04 Jul 17   0.1   - Initial version - MEJT
#
  local _prompt _default _response

  if [ "$1" ]; then _prompt="$1"; else _prompt="Are you sure"; fi
  _prompt="$_prompt [y/n] ?"

  # Loop forever until the user enters a valid response (Y/N or Yes/No).
  while true; do
    read -r -p "$_prompt " _response
    case "$_response" in
      [Yy][Ee][Ss]|[Yy]) # Yes or Y (case-insensitive).
        return 0
        ;;
      [Nn][Oo]|[Nn])  # No or N.
        return 1
        ;;
      *) # Anything else (including a blank) is invalid.
        ;;
    esac
  done
}

###############################
# Variables
#######################################################################
# Variables that you may want to update are marked #CONSIDER_UPDATING
#######################################################################

# Google Project Variables
# -------------------------
export GCP_PROJECT_ID=$(gcloud config list --format 'value(core.project)' 2>/dev/null)
# export PROJECT_ID='portsmouth-school-department'

# Google Cloud Storage Variables
# -------------------------------
# Note - there is one variable set later after the user input

export PG_DUMP_FILE_NAME_PREFIX="edfi_ods_green_pg_dump"

# Google Cloud SQL Variables
# ---------------------------
# The id for the instance of Cloud SQL postgres instance. For SY21, this is typically 'edfi-ods'
export CLOUD_SQL_INSTANCE_ID="edfi-ods"

export DATABASE_TO_EXPORT="EdFi_ODS_Green"

export BACKUP_DB_NAME="EdFi_ODS_SY21"

export CLOUD_SQL_SA_EMAIL=$(gcloud sql instances describe ${CLOUD_SQL_INSTANCE_ID} | grep serviceAccountEmailAddress| sed 's/^serviceAccountEmailAddress: //')

export EDFI_VERSION="v3.4"

####################################################
# Prompt user for input
####################################################
echo "Enter the value for the Google Cloud Storage bucket where the '${DATABASE_TO_EXPORT}' backup should be stored: "
read GCS_BUCKET_BACKUPS

export BUCKET_PATH=$GCS_BUCKET_BACKUPS/yearly_rollover_backups/SY21

####################################################
# Prompt user for confirmation before starting work
####################################################
echo ""
echo "Confirm the following values are correct:"
echo "- GCP_PROJECT_ID: ${GCP_PROJECT_ID}"
echo "- GCS Bucket name where SQL backup will be stored: ${GCS_BUCKET_BACKUPS}"

confirm 'Is this correct'

if [ $? -eq 1 ]
then echo '' && echo 'If the project is incorrect, run "gcloud config set project" to set the correct values and rerun this script'; exit 0
fi

echo ""
echo "This script will carry out the following":
echo "- Temporarily grant access to the Cloud SQL service account to read and write data to the '${GCS_BUCKET_BACKUPS}' bucket"
echo "- Dump the '${DATABASE_TO_EXPORT}' database to Google Cloud Storage in the following folder 'gs://${BUCKET_PATH}'"
echo "- Restore that SQL dump to a new Cloud SQL database: '${BACKUP_DB_NAME}'"
echo "- Delete the '${DATABASE_TO_EXPORT}' database and recreate it (for a blank ODS for the new school year)"
echo "- Restore the schema for Ed-Fi ${EDFI_VERSION} to the recreated '${DATABASE_TO_EXPORT}'"
echo ""
echo "NOTE: This script will pause partway through so that you can verify that the '${BACKUP_DB_NAME}' has been copied correctly before deleting the '${DATABASE_TO_EXPORT}' database."

echo ""
confirm 'This process will backup and then DESTROY the original ODS database. Are you ready to proceed'

if [ $? -eq 1 ]
then echo '' && echo 'If these values are incorrect, you may need to edit the "Variables" section of the script'; exit 0
fi

#####################################
# Carry out work
#####################################
 echo "Granting Cloud SQL Service Account access to write/read from gs://${GCS_BUCKET_BACKUPS}..."
 gsutil iam ch serviceAccount:${CLOUD_SQL_SA_EMAIL}:objectCreator,objectViewer gs://$GCS_BUCKET_BACKUPS


 export PG_DUMP_GCS_PATH=gs://${BUCKET_PATH}/${PG_DUMP_FILE_NAME_PREFIX}_$(date --utc +%Y%m%d_%H%M%SZ).sql.gz
 echo "Dumping SQL files for '${DATABASE_TO_EXPORT}' to ${PG_DUMP_GCS_PATH}..."

 gcloud sql export sql ${CLOUD_SQL_INSTANCE_ID} ${PG_DUMP_GCS_PATH} --database=$DATABASE_TO_EXPORT

 # create a new database
 echo "Creating new database: ${BACKUP_DB_NAME}..."
 gcloud sql databases create ${BACKUP_DB_NAME} --instance=${CLOUD_SQL_INSTANCE_ID}

 # restore the backup to the new database
 echo "Restoring backup at '${PG_DUMP_GCS_PATH}' to database '${BACKUP_DB_NAME}'..."
 gcloud sql import sql ${CLOUD_SQL_INSTANCE_ID} ${PG_DUMP_GCS_PATH} --database=${BACKUP_DB_NAME}

################################################################
# Confirm that the DATABASE_TO_EXPORT is ready to be destroyed
################################################################
echo ""
echo "Please run some queries against the '${BACKUP_DB_NAME}' database to confirm that it has the expected data in it."

confirm "Have you checked the data and are you ready to proceed to deleting and recreating the '${DATABASE_TO_EXPORT}' - NOTE: THIS CAN NOT BE UNDONE"

if [ $? -eq 1 ]
then echo '' && echo 'Script exiting...'; exit 0
fi

# get a new version of the DATABASE_TO_EXPORT to use for this school year
# delete the DATABASE_TO_EXPORT
echo ""
gcloud sql databases delete ${DATABASE_TO_EXPORT} --instance ${CLOUD_SQL_INSTANCE_ID}

gcloud sql databases create ${DATABASE_TO_EXPORT} --instance=${CLOUD_SQL_INSTANCE_ID}

# restore the backup to the new database
gcloud sql import sql ${CLOUD_SQL_INSTANCE_ID} "gs://landing_zone_public_resources/edfi_ods_dumps/${EDFI_VERSION}/edfi_ods_green" --database=${DATABASE_TO_EXPORT}

###########
# CLEAN UP
###########
# remove the permissions to access the data in the storage bucket
echo "Removing access for Cloud SQL service account for gs://${GCS_BUCKET_BACKUPS}"
gsutil iam ch -d serviceAccount:"${CLOUD_SQL_SA_EMAIL}" "gs://$GCS_BUCKET_BACKUPS"
