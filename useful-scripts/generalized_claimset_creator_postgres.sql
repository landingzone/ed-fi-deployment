-- This file was provided by Ed-Fi developer steven.arnold@ed-fi.org
do $$
declare
   	v_applicationId integer; 
   	v_claimSetId integer;
	v_resourceName VARCHAR(2048);
	v_claimSetName VARCHAR(255);
begin
	v_claimSetName = 'Test TPDM Claimset'; -- Replace this with the new Claimset name
    
	SELECT ApplicationId INTO v_applicationId 
	FROM dbo.Applications WHERE ApplicationName = 'Ed-Fi ODS API';
    
	INSERT INTO dbo.ClaimSets (ClaimSetName, Application_ApplicationId)
	VALUES (v_claimSetName, v_applicationId);
    
	SELECT ClaimSetId INTO v_claimSetId
	FROM dbo.ClaimSets 
	WHERE ClaimSetName = v_claimSetName;
    
	INSERT INTO dbo.ClaimSetResourceClaims
    (Action_ActionId
    ,ClaimSet_ClaimSetId
    ,ResourceClaim_ResourceClaimId
    ,AuthorizationStrategyOverride_AuthorizationStrategyId
    ,ValidationRuleSetNameOverride)
	SELECT ac.ActionId, v_claimSetId, ResourceClaimId, null, null 
	FROM dbo.ResourceClaims
	CROSS JOIN LATERAL (SELECT ActionId
					   FROM dbo.Actions
					   WHERE ActionName in ('Create', 'Read', 'Update', 'Delete')) as ac
   	WHERE ResourceName IN ('teacherCandidate', 'evaluationRating'); -- Replace teacherCandidate and evaluationRating with the resources for Claimset
end; 
$$
