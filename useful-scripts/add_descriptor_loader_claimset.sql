DO $$
<<edfi_block>>
DECLARE
	claim_set_name varchar := 'Landing Zone Descriptor Loader';
BEGIN

    -- clean up any existing prior versions of the claimset
    DELETE FROM dbo.claimsetresourceclaims WHERE claimset_claimsetid = (
        SELECT claimsetid FROM dbo.claimsets WHERE claimsetname = claim_set_name);

    DELETE FROM dbo.claimsets WHERE claimsetname = claim_set_name;

	INSERT INTO dbo.claimsets (claimsetname, application_applicationid)
	VALUES (claim_set_name, 1);  -- 1 is the application ID for the Ed-Fi ODS API

	-- create
	INSERT INTO dbo.claimsetresourceclaims (
        action_actionid,
        claimset_claimsetid,
	    resourceclaim_resourceclaimid,
	    authorizationstrategyoverride_authorizationstrategyid
	)
	SELECT
	    1, -- create
	    claimsetid,
	    2, -- system descriptors
	    1 -- NoFurtherAuthorizationRequired
	FROM dbo.claimsets
	WHERE claimsetname = claim_set_name;

	-- update
	INSERT INTO dbo.claimsetresourceclaims (
        action_actionid,
        claimset_claimsetid,
	    resourceclaim_resourceclaimid,
	    authorizationstrategyoverride_authorizationstrategyid
	)
	SELECT
	    3, -- update
	    claimsetid,
	    2, -- system descriptors
	    1 -- NoFurtherAuthorizationRequired
	FROM dbo.claimsets
	WHERE claimsetname = claim_set_name;

END edfi_block $$;